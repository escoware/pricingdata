
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'[dbo].[PricingNormal]') is not NULL
DROP TABLE [dbo].[PricingNormal]
GO

CREATE TABLE [dbo].[PricingNormalProfiles](
	[Utility] [VARCHAR](255) NULL,
	[LoadProfile] [VARCHAR](50) NOT NULL,
	[Month] [INT] NULL,
	[IntervalType] [VARCHAR](50) NOT NULL,
	[mWh] [DECIMAL](12, 6) NULL
) ON [PRIMARY]
GO


insert into [dbo].[PricingNormal] ( Utility,LoadProfile,Month,IntervalType,mWh)
select
 Utility
,LoadProfile
,Month
,IntervalType
,mWh = sum(mWh)
from dbo.Normal_mWh
cross apply IntervalType(CalendarDate,Hour)
group by
 Utility
,LoadProfile
,Month
,IntervalType

