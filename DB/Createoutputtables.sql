USE [PricingData]
GO

/****** Object:  Table [dbo].[SomeTable]    Script Date: 2/17/2020 8:15:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Blockoutput](
	[Utility] [VARCHAR](255) NULL,
	[LoadProfile] [VARCHAR](50) NOT NULL,
	[Month] [INT] NULL,
	[IntervalType] [VARCHAR](50) NOT NULL,
	[mWh] [DECIMAL](12, 6) NULL
) ON [PRIMARY]
GO


